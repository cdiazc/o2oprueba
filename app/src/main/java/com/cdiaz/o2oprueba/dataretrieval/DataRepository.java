package com.cdiaz.o2oprueba.dataretrieval;

import com.cdiaz.o2oprueba.dataretrieval.remote.ApiManager;
import com.cdiaz.o2oprueba.skeleton.presenters.contracts.GenericContract;
import com.cdiaz.o2oprueba.skeleton.views.activities.MainActivity;

/**
 * Created by carlos on 20/03/2018.
 */

public class DataRepository {

    private ApiManager apiManager;

    public DataRepository(final MainActivity activity) {
        apiManager = new ApiManager(activity);
    }

    public void getRecipe(final String query, final int page, final GenericContract callbackContainer) {

        //This is a good place to work on caching :)

        apiManager.getRecipe(query, page, callbackContainer);

    }

    public void cancelRequests() {

        apiManager.cancelRequests();
    }

    public void disconnect() {
        apiManager.disconnect();
    }



}
