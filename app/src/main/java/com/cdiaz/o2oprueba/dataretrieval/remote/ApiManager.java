package com.cdiaz.o2oprueba.dataretrieval.remote;


import com.cdiaz.o2oprueba.R;
import com.cdiaz.o2oprueba.skeleton.models.remote.PuppyResponse;
import com.cdiaz.o2oprueba.skeleton.presenters.contracts.GenericContract;
import com.cdiaz.o2oprueba.skeleton.views.activities.MainActivity;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by carlos on 20/03/2018.
 */

public class ApiManager {

    private MainActivity activity;

    private final OkHttpClient client = new OkHttpClient.Builder().build();

    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://www.recipepuppy.com/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private final ApiController api = retrofit.create(ApiController.class);

    public ApiManager(final MainActivity activity) {
        this.activity = activity;
    }

    public void cancelRequests() {
        client.dispatcher().cancelAll();
    }

    public void getRecipe(final String query, final int page, final GenericContract callbackContainer) {

        final Call<PuppyResponse> call = api.getRecipe(query, page);

        call.enqueue(new Callback<PuppyResponse>() {
            @Override
            public void onResponse(final Call<PuppyResponse> call, final Response<PuppyResponse> response) {

                if (response.code() < 400) {
                    callbackContainer.onSuccess(response.body());
                }
                else {
                    callbackContainer.onError(activity.getResources().getString(R.string.generice_error));
                }
            }

            @Override
            public void onFailure(final Call<PuppyResponse> call, final Throwable t) {
                callbackContainer.onError(t.getMessage());
            }
        });


    }

    public void disconnect() {
        activity = null;
    }



}
