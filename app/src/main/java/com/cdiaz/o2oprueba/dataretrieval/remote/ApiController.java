package com.cdiaz.o2oprueba.dataretrieval.remote;

import com.cdiaz.o2oprueba.skeleton.models.remote.PuppyResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by carlos on 20/03/2018.
 */

public interface ApiController {

    @GET("api")
    Call<PuppyResponse> getRecipe(@Query("q") String q, @Query("p") int page);

}
