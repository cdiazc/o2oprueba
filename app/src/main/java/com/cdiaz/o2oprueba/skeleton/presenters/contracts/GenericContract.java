package com.cdiaz.o2oprueba.skeleton.presenters.contracts;

import com.cdiaz.o2oprueba.skeleton.models.remote.PuppyResponse;

/**
 * Created by carlos on 23/03/2018.
 */

public interface GenericContract {

    void onSuccess(PuppyResponse response);
    void onError(String message);

}
