package com.cdiaz.o2oprueba.skeleton.presenters;

import com.cdiaz.o2oprueba.dataretrieval.DataRepository;
import com.cdiaz.o2oprueba.skeleton.models.remote.PuppyRecipe;
import com.cdiaz.o2oprueba.skeleton.models.remote.PuppyResponse;
import com.cdiaz.o2oprueba.skeleton.presenters.contracts.MainActivityPresenterContract;
import com.cdiaz.o2oprueba.skeleton.views.contracts.MainActivityContract;

/**
 * Created by carlos on 23/03/2018.
 */

public class MainActivityPresenter implements MainActivityPresenterContract {

    private MainActivityContract view;
    private DataRepository repository;


    public MainActivityPresenter(final MainActivityContract view, final DataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    public void disconnect(){
        cancelRequests();
        view = null;
        repository = null;
    }

    public void getRecipes(final String query, final int page) {

        repository.getRecipe(query, page, this);

    }

    public void cancelRequests() {
        repository.cancelRequests();
    }


    @Override
    public void onSuccess(final PuppyResponse response) {

        view.addRecipes(response.recipes);
    }

    @Override
    public void onError(final String message) {

        view.showToast(message);
    }

    public void openRecipe(final PuppyRecipe recipe) {

        view.openRecipe(recipe);

    }
}
