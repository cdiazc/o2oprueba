package com.cdiaz.o2oprueba.skeleton.models.remote;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by carlos on 23/03/2018.
 */

public class PuppyRecipe implements Serializable{

    @SerializedName("title")
    public String title;


    @SerializedName("href")
    public String href;


    @SerializedName("ingredients")
    public String ingredients;


    @SerializedName("thumbnail")
    public String thumbnail;

}
