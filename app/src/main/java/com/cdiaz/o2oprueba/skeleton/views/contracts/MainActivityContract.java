package com.cdiaz.o2oprueba.skeleton.views.contracts;

import com.cdiaz.o2oprueba.skeleton.models.remote.PuppyRecipe;

import java.util.List;

/**
 * Created by carlos on 23/03/2018.
 */

public interface MainActivityContract {

     void addRecipes(List<PuppyRecipe> recipeList);
     void showToast(String message);
     void openRecipe(PuppyRecipe recipe);


}
