package com.cdiaz.o2oprueba.skeleton.views.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Toast;

import com.cdiaz.o2oprueba.R;
import com.cdiaz.o2oprueba.adapters.DataAdapter;
import com.cdiaz.o2oprueba.databinding.ActivityMainBinding;
import com.cdiaz.o2oprueba.dataretrieval.DataRepository;
import com.cdiaz.o2oprueba.skeleton.models.remote.PuppyRecipe;
import com.cdiaz.o2oprueba.skeleton.presenters.MainActivityPresenter;
import com.cdiaz.o2oprueba.skeleton.views.contracts.MainActivityContract;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainActivityContract {

    private MainActivityPresenter presenter;
    private DataRepository repository;

    private RecyclerView recyclerView;
    private DataAdapter adapter;

    private ActivityMainBinding binding;

    private String lastQuery = "";
    private int lastPage = 1;

    private Snackbar loadingSnackbar;

    private boolean firstScroll = true;  //Marks if the first instance of artificial scrolling has been done
                                         //this first fake scroll is caused when screens are too big and the default
                                         //amount of items are smaller than the screen itself, this will cause
                                         //loading more items just when starting the app.

    private static final String LAST_QUERY_TAG = "MainActivity_LAST_QUERY_TAG";

    public static final String RECIPE_TAG = "MainActivity_RECIPE_TAG";


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            lastQuery = savedInstanceState.getString(LAST_QUERY_TAG, "");
        }

        setPresenter();
        setViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getRecipes(lastQuery, 1);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {

        outState.putString(LAST_QUERY_TAG, lastQuery);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        binding.activityMainResultsContainer.setOnScrollListener(null);
        binding.activityMainResultsSearch.addTextChangedListener(null); //avoid unwanted calls from this point on
        adapter.disconnect();
        presenter.disconnect();
        repository.disconnect();

        loadingSnackbar = null;
        presenter = null;
        repository = null;
        recyclerView = null;
        adapter = null;
        binding = null;
        lastQuery = null;
    }


    private void setPresenter() {
        repository = new DataRepository(this);
        presenter = new MainActivityPresenter(this, repository);

    }

    private void setViews() {

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        loadingSnackbar = Snackbar.make(binding.getRoot(), getString(R.string.main_activity_loading_more), Snackbar.LENGTH_INDEFINITE);

        recyclerView = binding.activityMainResultsContainer;

        adapter = new DataAdapter(new ArrayList<PuppyRecipe>(), presenter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int pastVisibleItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                    presenter.getRecipes(lastQuery, ++lastPage);
                    if (!firstScroll) {
                        loadingSnackbar.show();
                    }
                }
                firstScroll = false;
            }
        });

        binding.activityMainResultsSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(final Editable editable) {

                lastQuery = editable.toString();
                lastPage = 1;
                firstScroll = true;
                presenter.getRecipes(lastQuery, lastPage);
            }

        });
    }

    @Override
    public void addRecipes(final List<PuppyRecipe> recipeList) {

        if (lastPage == 1) {
            adapter.clearData();
        }
        else {
            loadingSnackbar.dismiss();
        }
        adapter.addData(recipeList);

    }

    @Override
    public void showToast(final String message) {

        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void openRecipe(final PuppyRecipe recipe) {

        final Intent i = new Intent(this, DetailActivity.class);
        i.putExtra(RECIPE_TAG, recipe);
        startActivity(i);

    }

}
