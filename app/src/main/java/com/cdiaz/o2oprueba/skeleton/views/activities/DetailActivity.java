package com.cdiaz.o2oprueba.skeleton.views.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.cdiaz.o2oprueba.R;
import com.cdiaz.o2oprueba.databinding.ActivityDetailBinding;
import com.cdiaz.o2oprueba.skeleton.models.remote.PuppyRecipe;

public class DetailActivity extends AppCompatActivity {

    ActivityDetailBinding binding;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);

        final Intent intent = getIntent();
        final PuppyRecipe recipe = (PuppyRecipe) intent.getSerializableExtra(MainActivity.RECIPE_TAG);

        binding.setRecipe(recipe);
        binding.setActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }


    public void openHref(final String url) {
        final Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}
