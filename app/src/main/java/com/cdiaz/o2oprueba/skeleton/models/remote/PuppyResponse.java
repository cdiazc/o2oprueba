package com.cdiaz.o2oprueba.skeleton.models.remote;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by carlos on 23/03/2018.
 */

public class PuppyResponse {

    @SerializedName("results")
    public List<PuppyRecipe> recipes;

}
