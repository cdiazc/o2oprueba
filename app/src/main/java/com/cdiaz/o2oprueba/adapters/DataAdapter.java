package com.cdiaz.o2oprueba.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.cdiaz.o2oprueba.R;
import com.cdiaz.o2oprueba.databinding.DataElementBinding;
import com.cdiaz.o2oprueba.skeleton.models.remote.PuppyRecipe;
import com.cdiaz.o2oprueba.skeleton.presenters.MainActivityPresenter;

import java.util.List;

/**
 * Created by carlos on 20/03/2018.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataViewHolder> {

    private MainActivityPresenter presenter;

    private List<PuppyRecipe> data;

    public DataAdapter(final List<PuppyRecipe> data, final MainActivityPresenter presenter) {
        this.data = data;
        this.presenter = presenter;
    }

    public void setData(final List<PuppyRecipe> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void addData(final List<PuppyRecipe> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void clearData() {
        this.data.clear();
        notifyDataSetChanged();
    }

    @Override
    public DataViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        final DataElementBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.data_element, parent, false);
        binding.setPresenter(presenter);
        return new DataViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        final PuppyRecipe responseElement = data.get(position);
        holder.bind(responseElement);
    }

    @Override
    public int getItemCount() {
        int ret = 0;

        if (data != null) {
            ret = data.size();
        }

        return ret;
    }

    public void disconnect() {
        presenter = null;
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        DataElementBinding binding;

        void bind(final PuppyRecipe response) {
            binding.setRecipe(response);
            binding.executePendingBindings();
        }

        DataViewHolder(final DataElementBinding bindings) {
            super(bindings.getRoot());
            this.binding = bindings;
        }
    }
}
